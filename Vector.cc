// Implementation of the templated Vector class
// ECE4893/8893 lab 3
// Ning Wang

#include <iostream> // debugging
#include <cstdlib>
#include <assert.h>
#include "Vector.h"

// Your implementation here
// Fill in all the necessary functions below
using namespace std;

// Default constructor
template <typename T>
Vector<T>::Vector()
{
    elements = (T*) malloc(sizeof(T));
    count = 0;
    reserved = 1;
}

// Copy constructor
template <typename T>
Vector<T>::Vector(const Vector& rhs)
{
    int size = rhs.Size();
    elements = (T*) malloc(size * sizeof(T));
    count = size; 
    reserved =  size;
    for (int i = 0; i < size; i++)
        new (elements + i) T(rhs[i]);
}

// Assignment operator
template <typename T>
Vector<T>& Vector<T>::operator=(const Vector& rhs)
{
    for (int i = 0; i <= this->Size(); i++)
        (elements + i)->~T();

    if (rhs.Size() < this->Size())
    {
        for (int i = 0; i < rhs.Size(); i++)
            new (elements + i) T(rhs[i]);
    }
    else
    {   // if size smaller than rhs, we need to dealloc ourselves
        // and alloc more memory
        free(elements);
        elements = (T*) malloc(rhs.Size() * sizeof(T));
        for (int i = 0; i < rhs.Size(); i++)
            new (elements + i) T(rhs[i]); 
        reserved = rhs.Size();
    }
    count = rhs.Size();
}

#ifdef GRAD_STUDENT
// Other constructors
template <typename T>
Vector<T>::Vector(size_t nReserved)
{   // Initialize with reserved memory
    elements = (T*) malloc(nReserved * sizeof(T));
    count = 0;
    reserved = nReserved;
}

template <typename T>
Vector<T>::Vector(size_t n, const T& t)
{   // Initialize with "n" copies of "t"
    elements = (T*) malloc(n * sizeof(T));
    for (int i = 0; i < n; i++)
        new (elements + i) T(t);
    count = n;
    reserved = n;
}

template <typename T>
void Vector<T>::Reserve(size_t n)
{   // Ensure enough memory for "n" elements
    if (reserved >= n)
        return;
    // Allocate more memory and copy existing elements
    T* new_elements = (T*) malloc(n * sizeof(T));
    for (int i = 0; i < count; i++)
        new (new_elements + i) T(elements[i]);

    // Free up original memory
    for (int i = 0; i < count; i++)
        elements[i].~T();
    free(elements);
    elements = new_elements;
    reserved = n;
}
#endif

// Destructor
template <typename T>
Vector<T>::~Vector()
{
    for (int i = 0 ; i < count; i++)
        elements[i].~T();
    free(elements);
}

// Add and access front and back
template <typename T>
void Vector<T>::Push_Back(const T& rhs)
{
    if (count < reserved)
        new (elements + count) T(rhs);
    else
    {   // not enough memory reserved, allocate more memory
        T* new_elements = (T*) malloc((1 + reserved) * sizeof(T));
        for (int i = 0; i < count; i++)
            new (new_elements + i) T(elements[i]);
        new (new_elements + count) T(rhs);
        for (int i = 0; i < count; i++)
            elements[i].~T();
        free(elements);
        elements = new_elements;
        reserved++;
    }
    count++;
}

template <typename T>
void Vector<T>::Push_Front(const T& rhs)
{
    if (count < reserved)
    {   // shift every elements to their right
        for (int i = count - 1; i >= 0; i--)
        {
            new (elements + i + 1) T(elements[i]);
            elements[i].~T(); // call destroy constructor for the old copy
        }
        new (elements) T(rhs);
    }
    else
    {   // not enough memory reserved, allocate more memory 
        T* new_elements = (T*) malloc((reserved + 1) * sizeof(T));
        for (int i = 0; i < count; i++)
            new (new_elements + i + 1) T(elements[i]);
        new (new_elements) T(rhs);
        for (int i = 0; i < count; i++)
            elements[i].~T();
        free(elements);
        elements = new_elements;
        reserved++;
    }
    count++;
}

template <typename T>
void Vector<T>::Pop_Back()
{   // Remove last element
    assert(count > 0); 
    count--;
    elements[count].~T();
}

template <typename T>
void Vector<T>::Pop_Front()
{   // Remove first element, shift every elements to their left
    assert(count > 0);
    elements[0].~T();
    for (int i = 1; i < count; i++)
    {
        new (elements + i - 1) T(elements[i]); 
        elements[i].~T();
    }
    count--;
}

// Element Access
template <typename T>
T& Vector<T>::Front() const
{
    return *elements;
}

// Element Access
template <typename T>
T& Vector<T>::Back() const
{
    return *(elements + count - 1);
}

template <typename T>
T& Vector<T>::operator[](size_t i)
{
    return *(elements + i);
}

template <typename T>
const T& Vector<T>::operator[](size_t i) const
{
    return *(elements + i);
}

template <typename T>
size_t Vector<T>::Size() const
{
    return count;
}

template <typename T>
bool Vector<T>::Empty() const
{
    return count == 0;
}

// Implement clear
template <typename T>
void Vector<T>::Clear()
{
    for(int i = 0; i < count; i++)
        elements[i].~T();
    count = 0;
}

// Iterator access functions
template <typename T>
VectorIterator<T> Vector<T>::Begin() const
{
  return VectorIterator<T>(elements);
}

template <typename T>
VectorIterator<T> Vector<T>::End() const
{
    return VectorIterator<T>(elements + count);
}

#ifdef GRAD_STUDENT
// Erase and insert
template <typename T>
void Vector<T>::Erase(const VectorIterator<T>& it)
{
    for (T* p = it.current; p != elements + count - 1; p++)
    {
        p->~T();
        new (p) T(*(p + 1));
    }
    this->Back().~T();
    count--;
}

template <typename T>
void Vector<T>::Insert(const T& rhs, const VectorIterator<T>& it)
{
    if (count < reserved)
    {
        for (T* p = it.current; p != elements + count; p++)
        {
            new (p + 1) T(*p);
            p->~T();
        }
        new (it.current) T(rhs);
    }
    else
    {
        T* new_elements = (T*) malloc((reserved + 1) * sizeof(T));
        int i;
        for (i = 0; elements + i != it.current; i++)
            new (new_elements + i) T(elements[i]);
        new (new_elements + i) T(rhs);
        for (; i != count; i++)
            new (new_elements + i + 1) T(elements[i]); 

        for (i = 0; i != count; i++)
            elements[i].~T();
        free(elements);
        elements = new_elements;
        reserved++; 
    }
    count++;
}
#endif

// Implement the iterators

// Constructors
template <typename T>
VectorIterator<T>::VectorIterator()
{
    current = NULL;
}

template <typename T>
VectorIterator<T>::VectorIterator(T* c)
{
    current = c;
}

// Copy constructor
template <typename T>
VectorIterator<T>::VectorIterator(const VectorIterator<T>& rhs)
{
    this->current = rhs.current;
}

// Iterator defeferencing operator
template <typename T>
T& VectorIterator<T>::operator*() const
{
    assert(current != NULL);
    return *(current);
}

// Prefix increment
template <typename T>
VectorIterator<T>  VectorIterator<T>::operator++()
{
    assert(current != NULL); 
    current++;
    return *this;
}

// Postfix increment
template <typename T>
VectorIterator<T> VectorIterator<T>::operator++(int)
{
    assert(current != NULL); 
    VectorIterator<T> original(*this);
    ++(*this);
    return original;
}

// Comparison operators
template <typename T>
bool VectorIterator<T>::operator !=(const VectorIterator<T>& rhs) const
{
    return current != rhs.current;
}

template <typename T>
bool VectorIterator<T>::operator ==(const VectorIterator<T>& rhs) const
{
    return current == rhs.current;
}
